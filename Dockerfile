from node:16-stretch as devbuild

ENV NODE_ENV=development

RUN mkdir /usr/src/app
WORKDIR /usr/src/app
COPY package.json /usr/src/app/
RUN npm install

COPY src /usr/src/app/src
COPY tsconfig.* /usr/src/app

RUN npx tsc -p .

####
from node:16-stretch as prodbuild

ENV NODE_ENV=production
COPY --from="devbuild" /usr/src/app/dist /usr/src/app/dist
copy package.json /usr/src/app/
copy env /usr/src/app/env

WORKDIR /usr/src/app
RUN npm install

CMD node /usr/src/app/dist/app
