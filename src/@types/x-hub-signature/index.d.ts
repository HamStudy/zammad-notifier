declare module 'x-hub-signature' {
  import type express from 'express-serve-static-core';

  export interface XHubMiddlewareOptions {
    algorithm: string;
    secret: string;
    require?: boolean;
    getRawBody?: (req: Express.Request) => any;
  }

  export interface XHubSignerOptions {
    algorithm: string;
    secret: string;
  }

  export function middleware(opts: XHubMiddlewareOptions): express.IRouterHandler;
  export namespace middleware {
    export function extractRawBody(...args: any[]): any;
  }

  export function signer(opts: XHubSignerOptions): (body: Buffer) => string;
}