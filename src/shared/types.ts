
export interface ZammadObject {
  id: number;
  active: boolean;

  created_at: string;
  created_by: string | ZammadUser;
  created_by_id: number;
  updated_at: string;
  updated_by: string | ZammadUser;
  updated_by_id: number;
}

export interface ZammadUser extends ZammadObject {
  verified: boolean;
  vip: boolean;
  login: string;
  email: string;
  firstname: string;
  lastname: string;
  image: string;
  organization: string;
  organization_id: number;
  out_of_office: boolean;
  role_ids: number[];
  roles: string[];
  address?: string;
  note?: string;
  phone?: string;
}

export interface ZammadArticle extends ZammadObject {
  attachments: any[];
  body: string;
  cc: string;
  content_type: string;

  from: string;
  in_reply_to: string;
  internal: boolean;
  message_id: number | null;
  message_id_md5: number | null;
  origin_by_id: number | null;

  preferences: any;
  references: null;
  reply_to: string | null;

  sender: string | ZammadUser;
  sender_id: number;

  subject: string;
  ticket_id: number;
  to: string;
  type: string;
  type_id: number;
  accounted_time: number;
}

export interface ZammadGroup extends ZammadObject {
  name: string;

  user_ids: number[];
  users: string[];

  follow_up_assignment: boolean;
  follow_up_possible: string;
}

export interface ZammadOrganization extends ZammadObject {
  name: string;
  note: string;
  shared: boolean;
  domain_assignment: boolean;

  member_ids: number[];
  members: string[];
}

export interface ZammadPriority extends ZammadObject {
  default_create: boolean;
  name: string;
}

export interface ZammadTicket extends ZammadObject {
  article_count: number;
  article_ids: number[];
  create_article_sender: string;
  create_article_sender_id: number;
  create_article_type: string;
  create_article_type_id: number;

  customer: ZammadUser;
  customer_id: number;

  group: ZammadGroup;
  group_id: number;

  last_contact_at: string;
  last_contact_customer_at: string;

  number: string;

  organization: ZammadOrganization;
  organization_id: number;

  owner: ZammadUser;
  owner_id: number;

  type?: null;

  priority: ZammadPriority;
  priority_id: number;
  state: string;
  state_id: number;
  ticket_time_accounting: any[];
  ticket_time_accounting_ids: number[];
  title: string;
}

export interface ZammadWebhookPayload {
  ticket?: ZammadTicket,
  article?: ZammadArticle,
}
