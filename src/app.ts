import 'module-alias/register';

import './pre-start'; // Must be the first import
import app from './Server';
import logger from '@shared/Logger';

process.on('unhandledRejection', (reason, p) => {
  console.log('Unhandled Rejection at: Promise', p, 'reason:', reason);
  // application specific logging, throwing an error, or other logic here
});
process.on('uncaughtException', function (err) {
  console.log('Caught unhandled exception: ', err);
  console.log("Stack: ", err.stack);
});

// Start the server
const port = Number(process.env.PORT || 3000);
app.listen(port, () => {
    logger.info('Express server started on port: ' + port);
});
