import { Router } from 'express';
import { addAsync } from '@awaitjs/express';
import { MessageEmbed, WebhookClient as DiscordWebhookClient } from 'discord.js';

import {middleware as XHubMiddleware} from 'x-hub-signature';
import { ZammadUser, ZammadWebhookPayload } from '@shared/types';
import TurndownService from 'turndown';
const turndown = new TurndownService()

const hashSecret = process.env.HASH_SECRET ?? 'examtools';

// User-route
const router = addAsync(Router());

const sigMiddleware = XHubMiddleware({
  algorithm: 'sha1',
  secret: hashSecret,
  require: false,
});

const rooms = {
  qtc: {
    id: '751202454019768331',
    token: 'jyj23dgupbi6vAhuvP605JNN1AWTfxKT7PIXa68ENaWfHeqBk8Arb1ju1X3vGCYHgCbH',
  },
  qrm: {
    id: '778851292658663445',
    token: 'D0iQJ89VSQXc66UA0-GYO0g1ieTi_Jj2k6d1zaxaEDFiAqWerbEmdEJJ_PXTnK3H_atw',
  },
  tickets: {
    id: '707764112867852328',
    token: 'GkaM8hJ-UktnfXdoOKbXsiFH5fNh7vrdVqGrKwo1Cp434cI7rtZbA2Y8_00pFTGWmK0O',
  }
}

const avatarURL = `https://exam.tools/images/avatars/examtools.png`;

async function postDiscordMessage(msg: string, embeds: MessageEmbed[], room: keyof typeof rooms) {
  const webHook = rooms[(room||'')] || rooms.tickets;
  try {
      const hook = new DiscordWebhookClient(webHook);
      const msgOpts = {
        embeds,
        avatarURL,
        content: msg,
        username: 'ExamTools Support',
      };
      console.log("Webhook message to discord: ", msgOpts);
      await hook.send(msgOpts);
      hook.destroy();
  } catch (err) {
      console.log("Webhook error:", err);
  }
}

const Colors = <const>{
  blue: '#0099ff',
  green: '#00ff99',
  yellow: '#ff9900',
  red: "#990000",
}

const hostnameDefault = 'support.exam.tools';

router.post('/zammad', sigMiddleware, async (req, res) => {
  let isUpdate = !!(<any>req.params).update;
  const body: ZammadWebhookPayload = req.body;

  const {ticket, article} = body;

  if (!ticket) {
    console.warn("Strange; no ticket data!", JSON.stringify(body));
    throw new Error("No ticket data!");
  }

  const ticketDate = new Date(ticket.created_at);
  if ((new Date().getTime() - ticketDate.getTime()) > 30000) {
    isUpdate = true;
  }
  const ticketEmbed = new MessageEmbed();

  let title = '';

  if (isUpdate && article) {
    const createUser = article.created_by as ZammadUser;
    // New comment:
    title = 'Comment added';
    ticketEmbed
      .setTitle(`#${ticket.id}: ${ticket.title}`)
      .setAuthor(`${createUser.firstname} ${createUser.lastname} <${createUser.email}>`)
      .setColor(Colors.yellow)
      .setDescription(turndown.turndown(article.body));
  } else {
    title = 'New ticket';
    ticketEmbed
      .setTitle(`#${ticket.id}: ${ticket.title}`)
      .setAuthor(`${ticket.owner.firstname} ${ticket.owner.lastname} <${ticket.owner.email}>`)
      .setColor(Colors.blue)
      .setDescription(turndown.turndown(article?.body || ''));
  }

  ticketEmbed
    .setURL(`https://${hostnameDefault}/#ticket/zoom/${ticket.id}`)
    .addFields(
      { name: 'Priority', value: ticket.priority.name, inline: true },
      { name: 'Type', value: ticket.type || 'Default', inline: true },
      { name: 'State', value: ticket.state || 'New', inline: true },
      // { name: 'Date', value: ticketDate.toISOString(), inline: true },
    )
    .setTimestamp(ticketDate);

  console.log("Webhook received: ", body);

  postDiscordMessage(title, [ticketEmbed], 'tickets');
  res.send('Okay');
});

// Export the base-router
export default router;
